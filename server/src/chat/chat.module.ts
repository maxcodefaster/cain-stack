import { Module, Inject, MiddlewareConsumer } from '@nestjs/common';
import { SuperloginModule } from 'src/core/superlogin-module';
import { superloginConfig } from 'src/core/superlogin-config';
import { ChatController } from './chat.controller';
import { StreamService } from 'src/core/services/stream/stream.service';
import { AuthService } from 'src/core/services/auth/auth.service';

@Module({
    imports: [
        SuperloginModule.forRoot(superloginConfig),
    ],
    controllers: [ChatController],
    providers: [StreamService, AuthService],
})
export class ChatModule {
    constructor(@Inject('superlogin') private superlogin: any) { }

    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(this.superlogin.requireAuth)
            .forRoutes('chat');
    }
}
