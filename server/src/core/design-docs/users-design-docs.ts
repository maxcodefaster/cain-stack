export const usersDesignDocuments = [
    {
        _id: '_design/userDoc',
        language: 'javascript',
        validate_doc_update: "function(newDoc, oldDoc, userCtx){function require(field, title) {title = title || 'Document must have a ' + field; if (!newDoc[field]){throw({forbidden : title}); } } function unchanged(field) {if (oldDoc && toJSON(oldDoc[field]) != toJSON(newDoc[field])){throw({forbidden : 'Field cant be changed: ' + field}); } } if(newDoc.type == 'privateDoc'){require('title'); require('author'); require('dateUpdated'); require('dateCreated'); unchanged('author'); unchanged('dateCreated'); log(userCtx); log(newDoc.author); if(('user:' + newDoc.author) !== userCtx.roles[0] && userCtx.roles[1] !== 'admin'){log('cant do this'); throw({forbidden: 'operation forbidden'}); } } }",
        views: {
            admin_users: {
                map: "function(doc){ if(doc.type == 'user' && doc.role == 'admin'){emit(doc);} }"
            },
            non_admin_users: {
                map: "function(doc){ if(doc.type == 'user' && doc.role != 'admin'){emit(doc);} }"
            },
            all_users: {
                map: "function(doc){ if(doc.type == 'user'){emit(doc);} }"
            }
        }
    }
]