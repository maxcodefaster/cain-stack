import { Module, Inject, MiddlewareConsumer } from '@nestjs/common';
import { UserController } from './user.controller';
import { SuperloginModule } from 'src/core/superlogin-module';
import { superloginConfig } from 'src/core/superlogin-config';
import { StreamService } from 'src/core/services/stream/stream.service';
import { AuthService } from 'src/core/services/auth/auth.service';

@Module({
    imports: [
        SuperloginModule.forRoot(superloginConfig),
    ],
    controllers: [UserController],
    providers: [StreamService, AuthService],
})
export class UserModule {
    constructor(@Inject('superlogin') private superlogin: any) { }

    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(this.superlogin.requireAuth)
            .forRoutes('user');
        consumer
            .apply(this.superlogin.requireAuth, this.superlogin.requireRole('admin'))
            .forRoutes('user/admin/');
    }
}
