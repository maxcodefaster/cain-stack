import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  isMobile = false;

  constructor(private platform: Platform) { }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
  }

}
