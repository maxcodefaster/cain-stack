import { Injectable, NgZone } from '@angular/core';
import { DataService } from '../core/data.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedDocService {
  // Define which database to access
  dbname = 'shared';
  sharedDocSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);
  singleUserDocSubject: BehaviorSubject<object[]> = new BehaviorSubject([]);

  constructor(private dataService: DataService, private zone: NgZone) { }

  init(): void {
    this.emitSharedDocs();
    this.dataService.dbs[this.dbname].changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
      if (change.doc.type === 'sharedDoc' || change.deleted) {
        this.emitSharedDocs();
      }
    });
  }

  initSingleUserDocs(id): void {
    this.emitSingleUserDocs(id);
    this.dataService.dbs[this.dbname].changes({ live: true, since: 'now', include_docs: true }).on('change', (change) => {
      if (change.doc.type === 'sharedDoc' || change.deleted) {
        this.emitSingleUserDocs(id);
      }
    });
  }

  getSharedDocs(): BehaviorSubject<object[]> {
    return this.sharedDocSubject;
  }

  getSingleUserDocs(): BehaviorSubject<object[]> {
    return this.singleUserDocSubject;
  }

  async saveSharedDocs(doc) {
    let returnMsg;
    // if doc already exists updated it
    if (doc.doc) {
      const updatedDoc = doc.doc;
      updatedDoc.title = doc.title;
      updatedDoc.note = doc.note;
      updatedDoc.dateUpdated = doc.dateUpdated;
      await this.dataService.updateDoc(updatedDoc, this.dbname).then(async (res) => {
        // if image was attached, add attachment
        if (doc.file) {
          if (doc.doc._attachments) {
            const attName = Object.keys(doc.doc._attachments)[0];
            // remove existing attachment
            await this.dataService.rmAttachment(res.id, attName, res.rev, this.dbname).then(async (res) => {
              // add attachment
              await this.dataService.addAttachment(res.id, doc.tile, res.rev, doc.file, this.dbname).then((res) => {
                returnMsg = res;
              }).catch((err) => { returnMsg = err; })
            }).catch((err) => { returnMsg = err; })
          }
        } else {
          returnMsg = res;
        }
      }).catch((err) => { returnMsg = err; });
    } else { // if doc does not exist, creat new one
      await this.dataService.createDoc({
        title: doc.title,
        author: doc.author,
        dateCreated: doc.dateCreated,
        dateUpdated: doc.dateUpdated,
        type: 'sharedDoc',
        note: doc.note,
      }, this.dbname).then(async (res) => {
        // if image was attached, add attachment
        if (doc.file) {
          await this.dataService.addAttachment(res.id, 'privateDocImg', res.rev, doc.file, this.dbname).then((res) => {
            returnMsg = res;
          }).catch((err) => { returnMsg = err; })
        } else {
          returnMsg = res;
        }
      }).catch((err) => { returnMsg = err; });
    }
    return returnMsg;
  }

  getAttachment(doc) {
    const attName = Object.keys(doc._attachments)[0];
    return this.dataService.getAttachment(doc._id, attName, this.dbname)
  }

  deleteSharedDoc(doc) {
    return this.dataService.deleteDoc(doc, this.dbname);
  }

  emitSingleUserDocs(id) {
    this.zone.run(() => {
      const options = {
        key: id,
        include_docs: true,
        descending: true
      };
      this.dataService.dbs[this.dbname].query('sharedDoc/by_user', options).then((data) => {
        const standardDoc = data.rows.map(row => {
          return row.doc;
        });
        this.singleUserDocSubject.next(standardDoc);
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  emitSharedDocs(): void {
    this.zone.run(() => {
      const options = {
        include_docs: true,
        descending: true
      };
      this.dataService.dbs[this.dbname].query('sharedDoc/by_date_created', options).then((data) => {
        const standardDoc = data.rows.map(row => {
          return row.doc;
        });
        this.sharedDocSubject.next(standardDoc);
      }).catch((err) => {
        console.log(err);
      });
    });
  }

}
