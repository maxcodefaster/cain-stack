import { Component, OnInit } from '@angular/core';
import { PrivateDocService } from '../services/private-doc.service';
import { ModalController, LoadingController, NavController, ActionSheetController, AlertController } from '@ionic/angular';
import { AuthService } from '../core/auth.service';
import { EditDocModal } from '../shared/modals/edit-doc/edit-doc.modal';
import { UserService } from '../core/user.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.page.html',
  styleUrls: ['./notes.page.scss'],
})
export class NotesPage implements OnInit {

  privateDocs: object[] = [];
  refreshing: boolean = false;
  searchControl: FormControl;
  loading: any;
  searching: boolean = false;
  profileImg;
  user: any;

  constructor(
    private privateDocService: PrivateDocService,
    private authService: AuthService,
    private userService: UserService,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private modalController: ModalController,
  ) { }

  ngOnInit() {
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then(async (res) => {
        this.privateDocService.init();
        await this.privateDocService.getPrivateDocs().subscribe(async (docs) => {
          this.privateDocs = docs;
          await this.getDocImgs(docs)
        });
        this.user = this.userService.currentUser;
        this.profileImg = await this.userService.getProfileImgLocal();
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  // Event Emitter of docList component
  onDocUpdate(updated: boolean) {
  }

  // ux refresh
  async doRefresh(event) {
    this.refreshing = true;
    this.profileImg = await this.userService.getProfileImgLocal();
    setTimeout(() => {
      this.refreshing = false;
      event.target.complete();
    }, 1000);
  }

  async getDocImgs(docs) {
    for (let doc of docs) {
      doc.userImg = this.profileImg;
      if (doc._attachments) {
        this.privateDocService.getAttachment(doc).then((res) => {
          doc.docImg = URL.createObjectURL(res);
        }).catch((err) => { console.log(err) });
      }
    }
  }

  async openModal(doc?) {
    const modal = await this.modalController.create({
      component: EditDocModal,
      swipeToClose: true,
      componentProps: {
        'doc': doc,
        'db': 'private'
      }
    }).then((modal) => {
      modal.present();
    });
  }

}
