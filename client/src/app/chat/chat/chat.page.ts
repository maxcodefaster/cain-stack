import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { LoadingController, NavController, IonContent, IonList } from '@ionic/angular';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ChatService } from 'src/app/core/chat.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit, AfterViewInit {

  @ViewChild(IonContent) contentArea: IonContent;
  @ViewChild(IonList, { read: ElementRef }) chatList: ElementRef;

  chatMsgs: Object[] = [];
  message: string = '';
  loading: any;
  currentUser;
  contactName: string = '';
  convoName: string;
  mutationObserver: any;

  constructor(
    private chatService: ChatService,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    public userService: UserService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.currentUser = this.userService.currentUser;
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.contactName = params.get('id');
          this.contactName ? null : this.navCtrl.navigateRoot('/app/conversations');
        });
        this.convoName = 'chat$' + this.chatService.createPrivateChatId(this.contactName);
        this.chatService.initConversation(this.contactName);
        this.chatService.getChat().subscribe((res) => {
          this.chatMsgs = res;
          if (this.chatMsgs.length === 0) {
            this.chatMsgs.push({
              author: this.contactName,
              msg: 'Looks like nobody is around. Type a message below to start chatting!'
            });
          }
        })
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  ngAfterViewInit() {
    this.mutationObserver = new MutationObserver((mutations) => {
      this.newChatAdded();
    });
    this.mutationObserver.observe(this.chatList.nativeElement, {
      childList: true
    });
  }

  newChatAdded(): void {
    this.contentArea.scrollToBottom();
  }

  preventDefault(event): void {
    event.preventDefault();
  }

  addChat(): void {
    if (this.message.length > 0) {
      let iso = this.getDateISOString();
      this.chatService.addMsg({
        msg: this.message,
        author: this.userService.currentUser.user_id,
        dateCreated: iso,
        chatDbId: this.convoName,
      });
      this.message = '';
    }
  }

  getDateISOString(): string {
    return new Date().toISOString();
  }

}
