import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/user.service';
import { AuthService } from 'src/app/core/auth.service';
import { NavController, LoadingController, Platform } from '@ionic/angular';
import { ChatService } from 'src/app/core/chat.service';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.page.html',
  styleUrls: ['./conversations.page.scss'],
})
export class ConversationsPage implements OnInit {

  currentUser;
  conversations: object[] = [];
  conversationIds = [];
  loading: any;
  isMobile: boolean = false;
  refreshing: boolean = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private chatService: ChatService,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    public platform: Platform,
  ) { }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.conversations = [];
        this.conversationIds = [];
        this.currentUser = this.userService.currentUser;
        this.chatService.initChatList();
        this.chatService.getChatList().subscribe(async (conversations) => {
          this.createChatList(conversations);
        });
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  async createChatList(chats) {
    for (let chat of chats) {
      if (!this.conversationIds.includes(chat._id) && (chat._id.indexOf(this.currentUser.user_id) !== -1)) {
        this.conversationIds.push(chat._id);
        if (chat.type === 'privateChat') {
          let participantId = chat.participants.filter(value => value !== this.currentUser.user_id)[0];
          await this.userService.getProfileImgRemote(participantId).subscribe((res) => {
            let profileImg = URL.createObjectURL(res);
            let chatObj = { id: chat._id, user: participantId, img: profileImg };
            this.conversations.push(chatObj);
          }, (err) => {
            let profileImg = '/assets/core/user.png';
            let chatObj = { id: chat._id, user: participantId, img: profileImg };
            this.conversations.push(chatObj);
          });
        }
      }
    }
  }

  // ux refresh
  async doRefresh(event) {
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
      event.target.complete();
    }, 1000);
  }


}
