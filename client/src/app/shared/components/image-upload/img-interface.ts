export interface ImgInterface {
    ratio: number;
    maintainAspectRation: boolean;
    roundCropper: boolean;
    blob: Blob;
}
