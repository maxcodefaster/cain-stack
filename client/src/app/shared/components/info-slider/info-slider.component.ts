import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, Platform } from '@ionic/angular';

@Component({
  selector: 'info-slider',
  templateUrl: './info-slider.component.html',
  styleUrls: ['./info-slider.component.scss'],
})
export class InfoSliderComponent implements OnInit {

  @ViewChild('slides') slides: IonSlides;
  isMobile = false;

  constructor(
    public platform: Platform,
  ) { }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
  }

  next() {
    this.slides.slideNext();
  }

  prev() {
    this.slides.slidePrev();
  }

}
