import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDocModal } from './edit-doc.modal';

describe('EditDocModal', () => {
  let component: EditDocModal;
  let fixture: ComponentFixture<EditDocModal>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDocModal ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDocModal);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
