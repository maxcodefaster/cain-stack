import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditDocModal } from './edit-doc.modal';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageUploadComponentModule } from '../../components/image-upload/image-upload.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ImageUploadComponentModule,
    ImageCropperModule,
    IonicModule,
  ],
  declarations: [EditDocModal],
})
export class EditDocModalModule { }
