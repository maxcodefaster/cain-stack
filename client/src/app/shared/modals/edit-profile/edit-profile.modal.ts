import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/core/user.service';
import { ImgInterface } from '../../components/image-upload/img-interface';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.modal.html',
  styleUrls: ['./edit-profile.modal.scss'],
})
export class EditProfileModal implements OnInit {

  user;
  loading: any;
  userName;
  profileForm: FormGroup;
  profileImg;
  changeImg = true;
  img: ImgInterface = {
    ratio: 1 / 1,
    maintainAspectRation: true,
    roundCropper: true,
    blob: undefined,
  };

  constructor(
    private modalCtrl: ModalController,
    private userService: UserService,
    private navParams: NavParams,
    public toastCtrl: ToastController,
    private fb: FormBuilder,
  ) {
    this.profileForm = this.fb.group({
      img: new FormControl('', []),
      bio: new FormControl('', []),
    });
  }

  ngOnInit() {
    this.user = this.navParams.get('user');
    if (this.user.bio) {
      this.profileForm.patchValue({
        bio: this.user.bio
      });
    }
    this.userName = this.user._id;
    if (this.user._attachments) {
      this.userService.getProfileImgRemote(this.userName).subscribe((res) => {
        this.img.blob = res;
      }, (error) => { console.log(error); });
    } else {
      this.img.blob = new Blob();
    }
    this.getProfileImgLocal();
  }

  async getProfileImgLocal() {
    this.profileImg = await this.userService.getProfileImgLocal();
  }

  saveForm() {
    if (this.profileForm.valid) {
      const form = this.profileForm.value;
      let postData: FormData = new FormData();
      let bio, img;
      let file: File;
      postData.append('id', this.userName)
      // check if profile img has been edited
      if (form.img !== '' && form.img !== null) {
        img = form.img;
        file = new File([form.img], 'file', { type: form.type });
        postData.append('file', file, 'profileImg');
      }
      // check if profile bio has been edited
      if (form.bio !== '') {
        bio = form.bio;
        postData.append('bio', bio)
      }
      // save profile
      this.userService.editUserProfile(postData).subscribe((res: any) => {
        this.changeImg = true;
        this.showToast('Updated Profile successfully');
        this.profileForm.reset();
        if (file) {
          this.userService.saveProfileImgLocal()
          this.profileImg = this.userService.createImgObjectUrl(img);
        }
        if (bio) {
          this.profileForm.patchValue({
            bio: bio
          });
        }
      }, error => {
        this.showToast(error.message)
        console.log(error);
      });
    } else {
      this.showToast('Fill out every field')
    }
  }

  async showToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      translucent: true,
      duration: 2000
    });
    toast.present();
  }

  close(): void {
    this.modalCtrl.dismiss();
  }

}
