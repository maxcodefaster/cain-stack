import { Component, OnInit } from '@angular/core';
import { LoadingController, NavController, AlertController, ActionSheetController, ModalController, ToastController } from '@ionic/angular';
import { AuthService } from '../../core/auth.service';
import { UserService } from '../../core/user.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { SharedDocService } from '../../services/shared-doc.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user;
  currentUser;
  loading: any;
  userName: string = 'User';
  bio: string = 'No bio';
  profileImg;
  refreshing: boolean = false;
  userDocs: object[] = [];

  constructor(
    private sharedDocService: SharedDocService,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private userService: UserService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.currentUser = this.userService.currentUser;
        this.route.paramMap.subscribe(async (params: ParamMap) => {
          this.userName = params.get('id');
          this.currentUser.user_id === this.userName ? this.navCtrl.navigateRoot('/app/me') : null;
          await this.getUserInfo();
          this.sharedDocService.singleUserDocSubject = new BehaviorSubject([]);
          this.sharedDocService.initSingleUserDocs(this.userName)
          this.sharedDocService.getSingleUserDocs().subscribe((docs) => {
            this.userDocs = docs;
          });
          this.loading.dismiss();
        });
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  // Event Emitter of docList component
  onDocUpdate(updated: boolean) { }

  // ux refresh
  async doRefresh(event) {
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
      event.target.complete();
    }, 1000);
  }

  getDocImgs(docs) {
    for (let doc of docs) {
      doc.userImg = this.profileImg;
      if (doc._attachments) {
        this.sharedDocService.getAttachment(doc).then((res) => {
          doc.docImg = URL.createObjectURL(res);
        }).catch((err) => { console.log(err) });
      }
    }
  }

  getUserInfo() {
    this.userService.getUserProfile(this.userName).subscribe((res: any) => {
      this.user = res;
      this.user.bio ? this.bio = this.user.bio : null;
      if (this.user._attachments) {
        this.userService.getProfileImgRemote(this.userName).subscribe((res) => {
          this.profileImg = URL.createObjectURL(res);
          this.getDocImgs(this.userDocs);
        }, (error) => { console.log(error); });
      } else {
        this.profileImg = 'assets/core/user.png';
        this.getDocImgs(this.userDocs);
      }
    }, (error) => { console.log(error); });
  }

  async comingSoonToast() {
    const toast = await this.toastCtrl.create({
      message: 'Feature coming soon™',
      translucent: true,
      duration: 2000
    });
    toast.present();
  }

}
