import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MePage } from './me.page';
import { SkeletonCardModule } from '../../shared/components/skeleton-card/skeleton-card.module';
import { DocListModule } from '../../shared/components/doc-list/doc-list.module';
import { EditDocModalModule } from '../../shared/modals/edit-doc/edit-doc.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

const routes: Routes = [
  {
    path: '',
    component: MePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DocListModule,
    EditDocModalModule,
    SkeletonCardModule,
    RouterModule.forChild(routes),
    LazyLoadImageModule
  ],
  declarations: [MePage]
})
export class MePageModule { }
