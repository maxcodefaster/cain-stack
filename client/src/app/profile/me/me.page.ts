import { Component, OnInit } from '@angular/core';
import { SharedDocService } from 'src/app/services/shared-doc.service';
import { LoadingController, NavController, AlertController, ActionSheetController, ModalController, Platform } from '@ionic/angular';
import { AuthService } from 'src/app/core/auth.service';
import { UserService } from 'src/app/core/user.service';
import { BehaviorSubject } from 'rxjs';
import { EditDocModal } from 'src/app/shared/modals/edit-doc/edit-doc.modal';

@Component({
  selector: 'app-me',
  templateUrl: './me.page.html',
  styleUrls: ['./me.page.scss'],
})
export class MePage implements OnInit {

  user;
  loading: any;
  userName: string = 'User';
  bio: string = 'No bio';
  profileImg;
  isMobile: boolean = false;
  refreshing: boolean = false;
  userDocs: object[] = [];

  constructor(
    private sharedDocService: SharedDocService,
    private loadingCtrl: LoadingController,
    private authService: AuthService,
    private userService: UserService,
    private navCtrl: NavController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private modalController: ModalController,
    public platform: Platform,
  ) { }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then((res) => {
        this.userName = this.userService.currentUser.user_id;
        this.getUserInfo();
        this.sharedDocService.singleUserDocSubject = new BehaviorSubject([]);
        this.sharedDocService.initSingleUserDocs(this.userName)
        this.sharedDocService.getSingleUserDocs().subscribe((docs) => {
          this.userDocs = docs;
        });
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  goToSettings() {
    this.navCtrl.navigateRoot(['/settings']);
  }

  // Event Emitter of docList component
  onDocUpdate(updated: boolean) { }

  // ux refresh
  async doRefresh(event) {
    this.refreshing = true;
    setTimeout(() => {
      this.refreshing = false;
      event.target.complete();
    }, 1000);
  }

  getDocImgs(docs) {
    for (let doc of docs) {
      doc.userImg = this.profileImg;
      if (doc._attachments) {
        this.sharedDocService.getAttachment(doc).then((res) => {
          doc.docImg = URL.createObjectURL(res);
        }).catch((err) => { console.log(err) });
      }
    }
  }

  getUserInfo() {
    this.userService.getUserProfile(this.userName).subscribe((res: any) => {
      this.user = res;
      this.user.bio ? this.bio = this.user.bio : null;
      if (this.user._attachments) {
        this.userService.getProfileImgRemote(this.userName).subscribe((res) => {
          this.profileImg = URL.createObjectURL(res);
          this.getDocImgs(this.userDocs);
        }, (error) => { console.log(error); });
      } else {
        this.profileImg = 'assets/core/user.png'
        this.getDocImgs(this.userDocs);
      }
    }, (error) => { console.log(error); });
  }

  async openModal(doc?) {
    const modal = await this.modalController.create({
      component: EditDocModal,
      swipeToClose: true,
      componentProps: {
        'doc': doc,
        'db': 'shared'
      }
    }).then((modal) => {
      modal.present();
    });
  }

}
