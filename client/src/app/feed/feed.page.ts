import { Component, OnInit } from '@angular/core';
import { SharedDocService } from '../services/shared-doc.service';
import { ModalController, LoadingController, NavController, ActionSheetController, AlertController, Platform } from '@ionic/angular';
import { AuthService } from '../core/auth.service';
import { EditDocModal } from '../shared/modals/edit-doc/edit-doc.modal';
import { UserService } from '../core/user.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {

  sharedDocs: object[] = [];
  refreshing: boolean = false;
  searchControl: FormControl;
  loading: any;
  searching: boolean = false;
  profileImg;
  user: any;
  isMobile: boolean = false;

  constructor(
    private sharedDocService: SharedDocService,
    private authService: AuthService,
    private userService: UserService,
    private loadingCtrl: LoadingController,
    public alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private navCtrl: NavController,
    private modalController: ModalController,
    public platform: Platform,
  ) { }


  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
    this.loadingCtrl.create({
      translucent: true,
      message: 'Authenticating...'
    }).then((overlay) => {
      this.loading = overlay;
      this.loading.present();
      this.authService.reauthenticate().then(async (res) => {
        this.user = this.userService.currentUser;
        this.sharedDocService.init();
        this.sharedDocService.getSharedDocs().subscribe(async (docs) => {
          this.sharedDocs = docs;
          await this.getDocImgs(docs)
        });
        this.profileImg = await this.userService.getProfileImgLocal();
        this.loading.dismiss();
      }, (err) => {
        this.loading.dismiss();
        this.navCtrl.navigateRoot('/login');
      });
    });
  }

  // Event Emitter of docList component
  onDocUpdate(updated: boolean) {
  }

  // ux refresh
  async doRefresh(event) {
    this.refreshing = true;
    this.profileImg = await this.userService.getProfileImgLocal();
    setTimeout(() => {
      this.refreshing = false;
      event.target.complete();
    }, 1000);
  }

  getDocImgs(docs) {
    for (let doc of docs) {
      this.userService.getProfileImgRemote(doc.author).subscribe((res) => {
        doc.userImg = URL.createObjectURL(res);
      }, (err) => {
        doc.userImg = '/assets/core/user.png';
      });
      if (doc._attachments) {
        this.sharedDocService.getAttachment(doc).then((res) => {
          doc.imgBlob = res;
          doc.docImg = URL.createObjectURL(res);
        }).catch((err) => { console.log(err) });
      }
    }
  }

  async openModal(doc?) {
    const modal = await this.modalController.create({
      component: EditDocModal,
      swipeToClose: true,
      componentProps: {
        'doc': doc,
        'db': 'shared'
      }
    }).then((modal) => {
      modal.present();
    });
  }

}
