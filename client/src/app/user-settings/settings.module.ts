import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SettingsPage } from './settings.page';
import { EditProfileModalModule } from '../shared/modals/edit-profile/edit-profile.module';
import { EditUserModalModule } from '../shared/modals/edit-user/edit-user.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';

const routes: Routes = [
  {
    path: '',
    component: SettingsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EditProfileModalModule,
    EditUserModalModule,
    ReactiveFormsModule,
    IonicModule,
    LazyLoadImageModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SettingsPage]
})
export class SettingsPageModule {}
