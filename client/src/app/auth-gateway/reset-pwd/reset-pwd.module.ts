import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ResetPwdPage } from './reset-pwd.page';
import { ErrorMessagesModule } from 'src/app/shared/components/error-messages/error-messages.module';

const routes: Routes = [
  {
    path: '',
    component: ResetPwdPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ErrorMessagesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResetPwdPage]
})
export class ResetPwdPageModule {}
